#!/bin/bash
################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################


# NEW SKU STYLE: MCM-C0001-1-V1-C3-M5
# family: {MCM-C0001}
# compute board: 1 (blue voxl)
# V1: family version 1
# Camera config 3 (hires stereo tracking)
# modem config 5


source /home/root/.profile.d/modalai_sku_definitions.sh

USER=$(whoami)

RESET_ALL="\e[0m"
RED="\e[91m"
YLW="\e[33m"
GRN="\e[32m"
SET_BOLD="\e[1m"

PRINT_ERROR (){
	echo -e "${RED}[ERROR] $@${RESET_ALL}"
}

PRINT_GREEEN_LINE (){
	echo -e "${GRN}${SET_BOLD}------------------------------------------------------------------${RESET_ALL}"
}

## These are the 4 main configuration states set by either a part number OR
## the production-specific arguments
_CONF_FAMILY=""
_CONF_BOARD=""
_CONF_CAM_NUM=""
_CONF_VERSION=""

## behavior modes
DRY_RUN=""
QUIET=""

print_usage () {
	clear -x
	PRINT_GREEEN_LINE
	echo -e "${GRN}${SET_BOLD}voxl-configure-sku${RESET_ALL}"
	echo -e ""
	echo -e "You provide this tool with a description of the product and it prints"
	echo -e "a formatted and validated SKU to stdout and to disk at "
	echo -e "$SKU_FILENAME for things like voxl-configure-mpa to read."
	echo -e ""
	echo -e "For human-interactive mode you likely want to use the wizard."
	echo -e ""
	echo -e "When running this in a script, you must at least provide the family"
	echo -e "name. All other args are optional. Things like camera configs and"
	echo -e "versions will be assumed to be default if not specified as an arg."
	echo -e ""
	echo -e "${GRN}${SET_BOLD}Arguments:${RESET_ALL}"
	echo -e "-b, --board    specify VOXL board code number or the name"
	echo -e "-c, --cam      specify the camera configuration"
	echo -e "-d, --dry-run  do everything except write sku to disk"
	echo -e "-f, --family   specify product family code number or name"
	echo -e "-h, --help     show this help text"
	echo -e "-q, --quiet    quiet mode, only write to disk"
	echo -e "-v, --version  specify product version"
	echo -e "-w, --wizard   run the wizard"
	echo -e ""
	echo -e "${GRN}${SET_BOLD}Families (code - name)${RESET_ALL}"
	voxl-print-family-table
	echo -e ""
	echo -e "${GRN}${SET_BOLD}VOXL Compute Boards (code - name)${RESET_ALL}"
	voxl-print-board-table
	echo -e ""
	echo -e "${GRN}${SET_BOLD}Camera Configs${RESET_ALL}"
	print-camera-configs
	echo -e ""
	echo -e "${GRN}${SET_BOLD}Examples:${RESET_ALL}"
	echo -e "voxl-configure-sku --wizard"
	echo -e "voxl-configure-sku --family starling -v 2"
	echo -e "voxl-configure-sku -f sentinel"
	echo -e "voxl-configure-sku -f fpv"
	echo -e "voxl-configure-sku -f MRB-D0008"
	PRINT_GREEEN_LINE
	exit 0
}



## validate and "fix" the family name argument to a valid code
_validate_family(){

	## already a valid code, nothing to do
	if voxl-is-valid-family-code $_CONF_FAMILY ; then
		return
	fi

	## check if it's a valid family name instead
	if ! voxl-is-valid-family-name $_CONF_FAMILY ; then
		PRINT_ERROR "Invalid Family Name: $_CONF_FAMILY, exiting"
		exit 1
	fi

	## convert the family name to code
	_CONF_FAMILY=$(voxl-family-name-to-code $_CONF_FAMILY)
}


## validate and "fix" the board argument to a number
_validate_board(){

	## already a valid code, nothing to do
	if voxl-is-valid-board-code $_CONF_BOARD ; then
		return
	fi

	## check if it's a valid family name instead
	if ! voxl-is-valid-board-name $_CONF_BOARD ; then
		PRINT_ERROR "Invalid board Name: $_CONF_BOARD, exiting"
		exit 1
	fi

	## convert the family name to code
	_CONF_BOARD=$(voxl-board-name-to-code $_CONF_BOARD)
}


_auto_select_board(){

	b=$(voxl-platform)

	case "$b" in
		"M0052")
			_CONF_BOARD=3
			;;
		"M0054")
			_CONF_BOARD=4
			;;
		"M0104")
			_CONF_BOARD=6
			;;
		*)
			## unknown, assume voxl1
			_CONF_BOARD=1
			;;
		esac
}

_suggest_default_cam_for_family(){
	case "$_CONF_FAMILY" in

		"MDK-F0001"|"MDK-F0002"|"MRB-D0001")
			echo -e "${SET_BOLD}The default camera config for M500 and Flight Deck is${RESET_ALL}"
			echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}3 (hires+stereo+tracking)${RESET_ALL}"
			;;

		"MCM-C0001"|"MRB-D0003")
			echo -e "${SET_BOLD}The default camera config for VOXLCAM and Seeker is${RESET_ALL}"
			echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}7 (TOF+stereo+tracking)${RESET_ALL}"
			;;

		"MRB-D0004"|"MRB-D0006")
			echo -e "${SET_BOLD}The default camera config for RB5-Flight and Sentinel is${RESET_ALL}"
			echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}11 (hires+tracking+dual_stereo)${RESET_ALL}"
			;;

		"MRB-D0005"|"MRB-D0011")
			echo -e "${SET_BOLD}The default camera config for Starling is${RESET_ALL}"
			echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}6 (hires+TOF+tracking)${RESET_ALL}"
			;;

		"MRB-D0008")
			echo -e "${SET_BOLD}The default camera config for FPV is${RESET_ALL}"
			echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}15 (dual_9782_tracking)${RESET_ALL}"
			;;

		"MRB-D0010")
			echo -e "${SET_BOLD}The default camera config for D0010 is${RESET_ALL}"
			echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}17 (hires+TOF+tracking+stereo)${RESET_ALL}"
			;;

		esac

}


_construct_sku_number_from_conf(){

	############################################################################
	## go through each name and populate the other _CONF_XYZ variables with
	## appropriate defaults if they are missing. Also do sanity and error checks
	############################################################################
	case "$_CONF_FAMILY" in

		"MDK-F0001") # Flight Deck with voxl+fc or voxl-flight
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="2"	# default to voxl-flight all-in-one board
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="3"	# default hires+stereo+tracking
			fi
			;;


		"MDK-F0002") # VOXL Deck with NO Flight Core!
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="1"	# default blue voxl board
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="3"	# default hires+stereo+tracking
			fi
			;;


		"MDK-F0006") # Flight Deck with voxl2
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="4"	# voxl2
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="11"	# default 6-cam setup
			fi
			;;


		"MRB-D0001") # M500

			if [ "$_CONF_BOARD" == "" ]; then
				PRINT_ERROR "for MRB-D0001 (M500), you must specify a board"
				PRINT_ERROR "-b 1 for VOXL1 with Flight Core"
				PRINT_ERROR "-b 2 for VOXL1 Flight all-in-one board"
				return 1
			fi
			if [[ ! "$_CONF_BOARD" == "1" ]] && [[ ! "$_CONF_BOARD" == "2" ]]; then
				PRINT_ERROR "invalid MRB-D0001 M500 board: $_CONF_BOARD"
				PRINT_ERROR "must be 1 or 2"
				return 1
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="3"	# default hires+stereo+tracking
			fi
			;;


		"MCM-C0001") # voxlcam
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="1"	# default blue voxl board
			fi
			if [[ ! "$_CONF_BOARD" == "1" ]]; then
				PRINT_ERROR "invalid MCM-C0001 (VOXLCAM) board: $_CONF_BOARD"
				PRINT_ERROR "must be 1 (blue VOXL1)"
				return 1
			fi
			if [ "$_CONF_VERSION" == "" ]; then
				_CONF_VERSION="2"	# default version 2
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="7"	# default tof+stereo+tracking
			fi
			;;


		"MRB-D0003") # Seeker V1
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="1"	# default blue voxl board
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="7"	# default tof+stereo+tracking
			fi
			;;


		"MRB-D0004") # Qualcomm rb5 flight 5g development drone
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="3"	# m0052 rb5 flight
			fi
			if [[ ! "$_CONF_BOARD" == "3" ]]; then
				PRINT_ERROR "invalid MRB-D0004 (RB5 Flight) board: $_CONF_BOARD"
				PRINT_ERROR "must be 3 (RB5)"
				return 1
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="11"	# default 6-cam setup
			fi
			;;


		"MRB-D0005"|"MRB-D0011") # starling V2

			if [ "$_CONF_BOARD" == "" ];
				then _CONF_BOARD="4"
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="6"	# default tof+tracking+hires
			fi

			;;


		"MRB-D0006") # Sentinel with voxl2
			if [ "$_CONF_BOARD" == "" ];
				then _CONF_BOARD="4"
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="11"	# default 6-cam setup
			fi
			;;

		"MRB-D0008") # FPV
			if [ "$_CONF_BOARD" == "" ];
				then _CONF_BOARD="4"
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="15"	# Stereo(OV9782) as separate mono cams
			fi
			;;

		"MRB-D0010") # starling V2 with stereo 9782
			if [ "$_CONF_BOARD" == "" ];
				then _CONF_BOARD="4"
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="17"	# default tof+tracking+hires+stereo
			fi
			;;

		"TF-M0054") # voxl2 test fixture
			if [ "$_CONF_BOARD" == "" ];
				then _CONF_BOARD="4"
			fi
			_CONF_VERSION="1"
			_CONF_CAM_NUM="11" # default 6-cam setup
			;;

		"TF-M0104") # voxl2-mini test fixture
			if [ "$_CONF_BOARD" == "" ];
				then _CONF_BOARD="6"
			fi
			_CONF_VERSION="1"
			_CONF_CAM_NUM="6" # hires, tof, tracking
			;;

		"MCCA-M0054") # voxl2 board only
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="4"
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="0"
			fi
			;;

		"MCCA-M0104") # voxl2-mini board only
			if [ "$_CONF_BOARD" == "" ];
				then _CONF_BOARD="6"
			fi
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="0"
			fi
			;;

		*)
			PRINT_ERROR "Invalid Family Code: ${_CONF_FAMILY}, valid family codes are:"
			voxl-print-family-table
			return 1
			;;
	esac


	if [ "$_CONF_VERSION" == "" ]; then
		_CONF_VERSION="1"		# default version 1
	fi


	## now make the SKU string
	if [[ "$_CONF_FAMILY" != *"MCCA"* ]]; then
		_CONF_SKU="${_CONF_FAMILY}-${_CONF_BOARD}"
		_CONF_SKU+="-V$_CONF_VERSION"
	else
		_CONF_SKU="${_CONF_FAMILY}"
	fi
	
	if [ "$_CONF_CAM_NUM" != "" ]; then
		if [ "$_CONF_CAM_NUM" == "custom" ]; then
			_CONF_CAM_NUM="C"
		fi
		_CONF_SKU+="-C$_CONF_CAM_NUM"
	fi
}





_wizard(){

	clear -x
	PRINT_GREEEN_LINE
	echo -e "            ${GRN}${SET_BOLD}Welcome to the voxl-configure-sku Wizard!${RESET_ALL}"
	echo -e ""


	## do a quick parse, this will migrate old filename to new in case
	if [ -f $SKU_FILENAME ]; then

		if voxl-inspect-sku --quiet; then
			echo "VOXL currently thinks it is in the following hardware:"
			voxl-inspect-sku
			echo "If this is what you want, select 1 (accept and continue) to"
			echo "leave it as-is. Otherwise, select the desired product family:"
			echo ""
		else
			echo ""
			echo "There was an issue parsing the currently saved SKU"
			echo "Please select a family to start making a new one:"
			echo ""
		fi
	else
		echo "VOXL is not yet configured for a particular SKU."
		echo "Please select the new desired product family:"
		echo ""
	fi


	##
	## Ask for the family
	##
	select _CONF_FAMILY in "accept and continue" "${VOXL_FAMILY_NAMES[@]}"; do
		case $_CONF_FAMILY in
		"accept and continue")
			echo "leaving SKU as-is"
			exit 0
			;;
		*)
			if ! voxl-is-valid-family-name $_CONF_FAMILY ; then
				echo "Invalid selection, try again"
			else
				echo "selected $_CONF_FAMILY"
				break
			fi
		esac
	done
	_validate_family



	##
	## Ask board name for old voxl1 products
	##
	if [ $_CONF_FAMILY == "MRB-D0001" ] || [ $_CONF_FAMILY == "MDK-F0001" ] || [ $_CONF_FAMILY == "MDK-F0002" ]; then
		PRINT_GREEEN_LINE
		echo ""
		echo "for M500 and flight deck, you must specify which board you are using"

		select _CONF_BOARD in "${VOXL_BOARD_NAMES[@]}" "quit"; do
			case $_CONF_BOARD in
			"quit")
				echo "quitting"
				exit 0
				;;
			*)
				if ! voxl-is-valid-board-name $_CONF_BOARD ; then
					echo "Invalid selection, try again"
				else
					echo "selected $_CONF_BOARD"
					break
				fi
			esac
		done
	elif [ $_CONF_FAMILY == "MCM-C0001" ] || [ $_CONF_FAMILY == "MRB-D0003" ]; then
		## voxlcams are all voxl1 boards
		_CONF_BOARD="1"
	else
		## for voxl2 nd newer, just auto detect this instead of asking the question
		_auto_select_board
	fi
	_validate_board


	##
	## ask for the camera config
	##
	PRINT_GREEEN_LINE
	echo -e ""
	echo -e "If you would like to select a special camera config that differs"
	echo -e "from the default for your product family, please select an option."
	echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}Simply hit ENTER to use the default${RESET_ALL}"
	echo -e ""
	print-camera-configs
	echo -e " C - use user-defined custom camera config in"
	echo -e "     (/data/modalai/custom_camera_config.txt)"
	echo -e " q - Quit The Wizard"
	echo -e ""
	_suggest_default_cam_for_family
	echo -e "${GRN}${SET_BOLD}${SET_UNDERLINE}Simply hit ENTER to use the default${RESET_ALL}"


	# read and validate camera config
	CAM_VALID=""
	while [ ! $CAM_VALID ]; do

		echo ""
		read -p "selection: " _CONF_CAM_NUM

		if [[ $_CONF_CAM_NUM == "c" ]] || [[ $_CONF_CAM_NUM == "C" ]] || [[ $_CONF_CAM_NUM == "custom" ]]; then
			echo "Selecting custom camera config"
			_CONF_CAM_NUM="C"
			CAM_VALID="true"
		elif [[ $_CONF_CAM_NUM =~ ^[0-9]+$ ]]; then
			echo "Selected numerical camera config"
			CAM_VALID="true"
		elif [[ $_CONF_CAM_NUM == "q" ]] || [[ $_CONF_CAM_NUM == "Q" ]]; then
			echo "quitting"
			exit 0;
		elif [[ $_CONF_CAM_NUM == "" ]]; then
			echo "Selecting default camera config"
			CAM_VALID="true"
		else
			echo "invalid entry, please try again"
		fi
	done

	## now do all the same validation and construction as would be done
	## with command line args
	if ! _construct_sku_number_from_conf; then
		echo ""
		echo "Would you like to restart the wizard?"
		echo ""
		read -p "Restart? y/n: " input
		case $input in
			[yY]*)
				echo 'Restarting'
				_wizard
				;;
			[nN]*)
				echo 'Ok, exiting'
				exit 0
				;;
			*)
				echo 'Invalid input' >&2
		esac
		
	fi

	## double check and print results
	PRINT_GREEEN_LINE
	echo ""
	echo "Your answers would construct the following SKU:"
	if ! voxl-inspect-sku --sku $_CONF_SKU; then
		exit 1
	fi



	echo "Would you like to continue? This sku will be saved"
	echo "to persistent memory so VOXL will remember what it"
	echo "is installed in, even between system image flashes."
	echo ""

	select input in "save and continue" "quit" "restart wizard"; do
		case $input in
		"save and continue")
			echo 'Continuing'
			break
			;;
		"quit")
			echo 'Quitting, nothing will be written to disk.'
			exit 0
			break
			;;
		"restart wizard")
			_wizard
			break
			;;
		*)
			echo "Invalid selection, try again"
		esac
	done



	## write to disk if the user said to continue
	mkdir -p /data/modalai/
	echo $_CONF_SKU > $SKU_FILENAME

	echo ""
	echo -e "${GRN}${SET_BOLD}DONE, $_CONF_SKU has been written to $SKU_FILENAME${RESET_ALL}"
	echo -e "${GRN}${SET_BOLD}Next you will likely want to run voxl-configure-mpa${RESET_ALL}"
	PRINT_GREEEN_LINE
	exit 0
}







_main(){

	if [ "$#" -eq 0 ]; then
		print_usage
		exit 1
	fi


	while (( "$#" )); do
		case "$1" in

		"-b"|"-B"|"--board"|"--BOARD") # compute board number
			if [ "$_CONF_BOARD" == "" ]; then
				_CONF_BOARD="$2"
				_validate_board
				shift
			else
				PRINT_ERROR "Recieved multiple attempts to set board number, exiting"
				exit 1
			fi
			;;

		"-c"|"-C"|"--cam"|"--CAM") # camera config number
			if [ "$_CONF_CAM_NUM" == "" ]; then
				_CONF_CAM_NUM="$2"
				shift
			else
				PRINT_ERROR "Recieved multiple attempts to set camera config number, exiting"
				exit 1
			fi
			if [ $_CONF_CAM_NUM == "c" ] || [ $_CONF_CAM_NUM == "C" ] || [ $_CONF_CAM_NUM == "custom" ]; then
				echo "Selecting custom camera config"
				_CONF_CAM_NUM="C"
			elif [[ $_CONF_CAM_NUM =~ ^[0-9]+$ ]]; then
				echo "Selected numerical camera config"
			else
				echo "invalid camera config, must be a number or C for custom"
				exit 1
			fi
			;;

		"-d"|"--dry-run")
			DRY_RUN=true
			;;

		"-f"|"-F"|"--family"|"--FAMILY") # family name
			if [ "$_CONF_FAMILY" == "" ]; then
				_CONF_FAMILY="$2"
				_validate_family
				shift
			else
				PRINT_ERROR "Recieved multiple attempts to set family name, exiting"
				exit 1
			fi
			;;

		"-h"|"--help")
			print_usage
			exit 0
			;;

		"-q"|"--quiet")
			QUIET=true
			;;

		"-v"|"-V"|"--version"|"--VERSION") # family version number
			if [ "$_CONF_VERSION" == "" ]; then
				_CONF_VERSION="$2"
				shift
			else
				PRINT_ERROR "Recieved multiple attempts to set camera version number, exiting"
				exit 1
			fi
			;;

		"-w"|"-W"|"wizard"|"--wizard")
			_wizard
			exit 0;
			;;
		*)
			PRINT_ERROR "Invalid arg: $1, exiting"
			exit 1
			;;

		esac
		shift
	done

	# no family given
	if [ ! $_CONF_FAMILY ]; then
		PRINT_ERROR "you must provide a family name or run the wizard"
		exit 1;
	fi

	_auto_select_board

	## now we are done validating all the config variables, make a sku number
	## and print the final config we ended up with
	if ! _construct_sku_number_from_conf; then
		exit 1
	fi

	## write to disk, and wipe the old file if it exists
	if [ ! $DRY_RUN ]; then
		if [ -f $FACTORY_FILENAME ]; then
			rm -f $FACTORY_FILENAME
			if [ ! $QUIET ]; then
				echo "${YLW}[WARNING] migrating $FACTORY_FILENAME to $SKU_FILENAME ${RESET_ALL}"
			fi
		fi
		if [ ! $QUIET ]; then
			echo -e "${GRN}[INFO] writing $_CONF_SKU to $SKU_FILENAME${RESET_ALL}"
		fi
		mkdir -p /data/modalai/
		echo $_CONF_SKU > $SKU_FILENAME
	fi

	# when not in quiet mode, print results
	if [ ! $QUIET ]; then
		voxl-inspect-sku --sku $_CONF_SKU
	fi

	exit 0
}




_main "$@"
